oneworld.StaticMap
==================

.. currentmodule:: oneworld

.. autoclass:: StaticMap

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~StaticMap.__init__
      ~StaticMap.add_choropleth
      ~StaticMap.add_colorbar
      ~StaticMap.add_legend
      ~StaticMap.add_markers
      ~StaticMap.add_shp
      ~StaticMap.add_txt
      ~StaticMap.get_projection
      ~StaticMap.plot_inner
      ~StaticMap.plot_outter
      ~StaticMap.plot_physics
      ~StaticMap.plot_pop
      ~StaticMap.savemap
   
   

   
   
   