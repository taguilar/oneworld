oneworld.WebMap
===============

.. currentmodule:: oneworld

.. autoclass:: WebMap

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~WebMap.__init__
      ~WebMap.add_basemap
      ~WebMap.add_box
      ~WebMap.add_choropleth
      ~WebMap.add_circles
      ~WebMap.add_geojson
      ~WebMap.add_heatmap
      ~WebMap.add_layer_control
      ~WebMap.add_lines
      ~WebMap.add_logo
      ~WebMap.add_network
      ~WebMap.add_overlay
      ~WebMap.add_panel
      ~WebMap.savemap
   
   

   
   
   