******************
Tutorials
******************

Static Map
==========

.. toctree::

   s_start
   s_markers
   s_choro

Web Map
=======

.. toctree::

   w_start
   w_circles
   w_nets
   w_json
   w_layers
   w_styles
   w_info
