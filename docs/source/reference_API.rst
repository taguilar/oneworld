API Reference
=============

Static Maps
-----------

.. rubric:: Map Object   

.. currentmodule:: oneworld

.. autosummary::
   :toctree: generated

   ~StaticMap

.. rubric:: Methods

.. autosummary::
   :toctree: generated

   ~StaticMap.get_projection
   ~StaticMap.plot_outter
   ~StaticMap.plot_inner
   ~StaticMap.plot_physics
   ~StaticMap.plot_pop
   ~StaticMap.add_txt
   ~StaticMap.add_markers
   ~StaticMap.add_legend
   ~StaticMap.add_choropleth
   ~StaticMap.add_colorbar
   ~StaticMap.add_shp
   ~StaticMap.savemap

Interactive Maps
----------------

.. currentmodule:: oneworld

.. rubric:: Map Object   

.. autosummary::
   :toctree: generated

   ~WebMap

.. rubric:: Methods

.. autosummary::
   :toctree: generated

   ~WebMap.add_basemap
   ~WebMap.add_box
   ~WebMap.add_choropleth
   ~WebMap.add_circles
   ~WebMap.add_geojson
   ~WebMap.add_heatmap
   ~WebMap.add_layer_control
   ~WebMap.add_lines
   ~WebMap.add_logo
   ~WebMap.add_network
   ~WebMap.add_overlay
   ~WebMap.add_panel
   ~WebMap.savemap
