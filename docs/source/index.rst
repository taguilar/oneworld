##################################
OneWorld: python mapping made easy
##################################

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   welcome
   tutorials
   reference_API

##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
