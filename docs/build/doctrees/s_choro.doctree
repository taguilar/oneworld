��K.      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Shapefiles and choroplets�h]�h	�Text����Shapefiles and choroplets�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�:/home/toni/Projects/repos/oneworld/docs/source/s_choro.rst�hKubh	�	paragraph���)��}�(h�;Let's start,as always, by creating our `StaticMap` object::�h]�(h�)Let’s start,as always, by creating our �����}�(hh/hhhNhNubh	�title_reference���)��}�(h�`StaticMap`�h]�h�	StaticMap�����}�(hh9hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh� object:�����}�(hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�literal_block���)��}�(h��import oneworld as ow
mymap = ow.StaticMap(view = [-125, -66.5, 21, 50],
                     central_longitude = -98.6,
                     projection = 'AlbersEqualArea')�h]�h��import oneworld as ow
mymap = ow.StaticMap(view = [-125, -66.5, 21, 50],
                     central_longitude = -98.6,
                     projection = 'AlbersEqualArea')�����}�hhSsbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve�uh+hQhh,hKhhhhubh.)��}�(h��This time we don't request any borders so that they don't interfere with
the polygons we will be adding.
To add the polygons contained in a shapefile, we can use the `add_shapefile`
method once the map has been created::�h]�(h��This time we don’t request any borders so that they don’t interfere with
the polygons we will be adding.
To add the polygons contained in a shapefile, we can use the �����}�(hhchhhNhNubh8)��}�(h�`add_shapefile`�h]�h�add_shapefile�����}�(hhkhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hhcubh�&
method once the map has been created:�����}�(hhchhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubhR)��}�(h�@mymap.add_shp(shp_file = 'us_state_20m.shp', edgecolor = 'grey')�h]�h�@mymap.add_shp(shp_file = 'us_state_20m.shp', edgecolor = 'grey')�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]�hahbuh+hQhh,hKhhhhubh.)��}�(hX  We only need to state the name of the shapefile (`us_state_20m.shp` in this
case), but we have also added the `edgecolor` keyword for extra flavor
(`add_shp` accepts the standard `matplotlib` keywords for plot aesthetics).
After saving the image, we obtain the following map:�h]�(h�1We only need to state the name of the shapefile (�����}�(hh�hhhNhNubh8)��}�(h�`us_state_20m.shp`�h]�h�us_state_20m.shp�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh�+ in this
case), but we have also added the �����}�(hh�hhhNhNubh8)��}�(h�`edgecolor`�h]�h�	edgecolor�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh� keyword for extra flavor
(�����}�(hh�hhhNhNubh8)��}�(h�	`add_shp`�h]�h�add_shp�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh� accepts the standard �����}�(hh�hhhNhNubh8)��}�(h�`matplotlib`�h]�h�
matplotlib�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh�T keywords for plot aesthetics).
After saving the image, we obtain the following map:�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�image���)��}�(h�R.. image:: _static/tut_shp1.png
   :alt: Map with added polygons from a shapefile
�h]�h}�(h!]�h#]�h%]�h']�h)]��alt��(Map with added polygons from a shapefile��uri��_static/tut_shp1.png��
candidates�}��*�h�suh+h�hhhhhh,hNubh.)��}�(h��If we want to include in our map only those polygons in the shapefile whose
attribute named "STUSPS" has a value of "CO", we can use the `subset`
and `shp_key` keywords::�h]�(h��If we want to include in our map only those polygons in the shapefile whose
attribute named “STUSPS” has a value of “CO”, we can use the �����}�(hh�hhhNhNubh8)��}�(h�`subset`�h]�h�subset�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh�
and �����}�(hh�hhhNhNubh8)��}�(h�	`shp_key`�h]�h�shp_key�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh�
 keywords:�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubhR)��}�(h�tmymap.add_shp(shp_file = 'us_state_20m.shp', edgecolor = 'grey',
              subset_key = 'STUSPS', subset = 'CO')�h]�h�tmymap.add_shp(shp_file = 'us_state_20m.shp', edgecolor = 'grey',
              subset_key = 'STUSPS', subset = 'CO')�����}�hj,  sbah}�(h!]�h#]�h%]�h']�h)]�hahbuh+hQhh,hKhhhhubh.)��}�(h�Which gives the following map:�h]�h�Which gives the following map:�����}�(hj:  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK!hhhhubh�)��}�(h�\.. image:: _static/tut_shp2.png
   :alt: Map with one single polygon added from a shapefile
�h]�h}�(h!]�h#]�h%]�h']�h)]��alt��2Map with one single polygon added from a shapefile��uri��_static/tut_shp2.png�h�}�h�jU  suh+h�hhhhhh,hNubh.)��}�(h��Since only one polygon in the `shapefile` fulfills our requirement, only one
polygon has been added. Note also that since we did not request any
border to be plot when creating the `StaticMap` object, the figure only
shows the polygon selected.�h]�(h�Since only one polygon in the �����}�(hjW  hhhNhNubh8)��}�(h�`shapefile`�h]�h�	shapefile�����}�(hj_  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hjW  ubh�� fulfills our requirement, only one
polygon has been added. Note also that since we did not request any
border to be plot when creating the �����}�(hjW  hhhNhNubh8)��}�(h�`StaticMap`�h]�h�	StaticMap�����}�(hjq  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hjW  ubh�4 object, the figure only
shows the polygon selected.�����}�(hjW  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK&hhhhubh.)��}�(hX[  To color each of the polygons in the `shapefile` according to some value,
i.e. to create a choropleth, the `add_choropleth` can be used, since it is
its only purpose. Let's add the polygons from the same `shapefile` of
the previous examples, and color them according to theeir value
in the column "Region" in the dataframe of a preloaded dataset::�h]�(h�%To color each of the polygons in the �����}�(hj�  hhhNhNubh8)��}�(h�`shapefile`�h]�h�	shapefile�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�; according to some value,
i.e. to create a choropleth, the �����}�(hj�  hhhNhNubh8)��}�(h�`add_choropleth`�h]�h�add_choropleth�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�S can be used, since it is
its only purpose. Let’s add the polygons from the same �����}�(hj�  hhhNhNubh8)��}�(h�`shapefile`�h]�h�	shapefile�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�� of
the previous examples, and color them according to theeir value
in the column “Region” in the dataframe of a preloaded dataset:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK+hhhhubhR)��}�(h��df = ow.load_dataset('farms')
mymap.add_choropleth(shp_file = 'us_state_20m.shp', data = df,
                     shp_key = 'GEOID', geoid = 'FIPS', color = 'Region',
                     edgecolor = 'grey')�h]�h��df = ow.load_dataset('farms')
mymap.add_choropleth(shp_file = 'us_state_20m.shp', data = df,
                     shp_key = 'GEOID', geoid = 'FIPS', color = 'Region',
                     edgecolor = 'grey')�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�hahbuh+hQhh,hK1hhhhubh.)��}�(h�4After saving the map, we obtain the following image:�h]�h�4After saving the map, we obtain the following image:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK6hhhhubh�)��}�(h�[.. image:: _static/tut_shp3.png
   :alt: Map with polygons colored according to some value
�h]�h}�(h!]�h#]�h%]�h']�h)]��alt��1Map with polygons colored according to some value��uri��_static/tut_shp3.png�h�}�h�j�  suh+h�hhhhhh,hNubh.)��}�(h��The `add-choropleth` method uses the "FIPS" column of the dataframe "df"
to uniquely identify each polygon in the shapefile through the "GEOID"
attribute, and then colors it according to the value found in the
"Region" column of the "df" dataframe.�h]�(h�The �����}�(hj�  hhhNhNubh8)��}�(h�`add-choropleth`�h]�h�add-choropleth�����}�(hj   hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�� method uses the “FIPS” column of the dataframe “df”
to uniquely identify each polygon in the shapefile through the “GEOID”
attribute, and then colors it according to the value found in the
“Region” column of the “df” dataframe.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK;hhhhubh.)��}�(h�?The `color` keyword accepts both categorical and numeric data::�h]�(h�The �����}�(hj  hhhNhNubh8)��}�(h�`color`�h]�h�color�����}�(hj   hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj  ubh�3 keyword accepts both categorical and numeric data:�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK@hhhhubhR)��}�(h��mymap.add_choropleth(shp_file = 'us_state_20m.shp', data = df,
                     shp_key = 'GEOID', geoid = 'FIPS',
                     color = 'Land in farms',
                     palette = 'YlGn', n_colors = 8, edgecolor = 'grey')�h]�h��mymap.add_choropleth(shp_file = 'us_state_20m.shp', data = df,
                     shp_key = 'GEOID', geoid = 'FIPS',
                     color = 'Land in farms',
                     palette = 'YlGn', n_colors = 8, edgecolor = 'grey')�����}�hj8  sbah}�(h!]�h#]�h%]�h']�h)]�hahbuh+hQhh,hKBhhhhubh�)��}�(h�[.. image:: _static/tut_shp4.png
   :alt: Map with polygons colored according to some value
�h]�h}�(h!]�h#]�h%]�h']�h)]��alt��1Map with polygons colored according to some value��uri��_static/tut_shp4.png�h�}�h�jS  suh+h�hhhhhh,hNubeh}�(h!]��shapefiles-and-choroplets�ah#]�h%]��shapefiles and choroplets�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�jZ  jW  s�	nametypes�}�jZ  �sh!}�jW  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.