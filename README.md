# OneWorld

Welcome to OneWorld, a python mapping library intended to make plotting
maps with python easy and accessible to everyone. It combines the
mapping power of
[cartopy] with
the versatility of [leaflet] and
the aesthetics and input structure of  [seaborn]
to create both static and interactive maps.

|            |                 |
|:----------:|:---------------:|
![](docs/build/html/_static/tut_shp4.png)|![](docs/build/html/_static/welcome.png)

OneWorld accepts data inputs as sequences (lists or tuples) or 
[pandas] dataframes, much in the same
way as
[seaborn] does. If you have used seaborn
before, you already know how inputs work in OneWorld. 

### Installation

The best way to install OneWorld is through the `conda-forge` conda channel:

```
$ conda install -c conda-forge oneworld
```

You can also install OneWorld through `pip`, provided you satisfy the
requirements listed below:

```
$ pip install oneworld
```

You'll need the following packages to install OneWorld through `pip`:

* [matplotlib] 3.6.0 (or later)
* [pandas] 1.5.2 (or later)
* [seaborn] 0.12.2 (or later)
* [cartopy] 0.21.1 (or later)
* [jinja2] 3.1.2 (or later)

You can find the source code in Bitbucket https://bitbucket.org/taguilar/oneworld

### Documentation

The full documentation packaged with OneWorld can be found
[here](docs/build/html/index.html). It can also be found
in Read the Docs at
[https://oneworld.readthedocs.io](https://oneworld.readthedocs.io).
It includes an introduction, tutorials and API reference.

### License

This software is distributed under a MIT license.

   [matplotlib]: https://matplotlib.org/3.2.0/index.html
   [cartopy]: https://scitools.org.uk/cartopy/docs/latest/
   [leaflet]: https://leafletjs.com
   [seaborn]: https://seaborn.pydata.org
   [pandas]: https://pandas.pydata.org/
   [jinja2]: https://jinja.palletsprojects.com/en/2.11.x/
   [fiona]: https://pypi.org/project/Fiona/

