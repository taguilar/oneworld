---
title: 'OneWorld: python mapping made easy'
tags:
  - Python
  - Geography
  - GIS
  - Engineering
  - Visualization
authors:
  - name: Antoni Aguilar-Mogas
    orcid: 0000-0002-5952-3629
    affiliation: "1, 2"
    corresponding: true
affiliations:
 - name: Department of Geography, East Carolina University, USA
   index: 1
 - name: Center for Health Disparities, East Carolina University, USA
   index: 2
date: 29 March 2023
bibliography: paper.bib
---

# Summary

In this paper I present `OneWorld`, a python module designed to make geo-mapping accessible
to all audiences. `OneWorld` is able to produce both publication-grade static maps and internet-ready interactive maps. The software uses a consistent input structure for both static and interactive maps. It is designed to make Geographic Information Systems (GIS) more approachable to a general audience that might not be familiar with such systems but still needs to represent geographical information in a simple and clear manner. `OneWorld` is able to represent point data (markers), area data (choropleths) and network data, or a combination of these representations. The data can be represented in different layers (which in the case of interactive maps can be activated and deactivated) and different background maps as basemaps (including physical and political maps from Natural Earth [@naturalearth] and street maps from OpenStreetMaps [@OSM]). It also supports the addition of custom shapes to the map (shapefiles for static maps and GeoJSON files for interactive maps). Finally, `OneWorld` uses traditional python data structures as input arguments for its methods such as lists, tuples and pandas DataFrames [@pandas]. A complete description of the software as well as tutorials and examples can be found online at [https://oneworld.readthedocs.io](https://oneworld.readthedocs.io).



# Statement of need

The necessity to plot the value of different variables on a map arises from the fact that several quantities (such as sociodemographic and economic variables) exhibit a strong geographical component.
Examples include the distribution of natural resources [@usda], the production potential of bio-based products [@biogas] and the social determinants of health in the study of health inequities [@inequities].
In this context, having the ability to represent clearly and precisely geospatial distributions is crucial. Moreover, these kind of studies often involve both numerical and categorical variables, which accentuates the importance of having a flexible software that is able to deal with different types of data.

Ever since the information explosion following the beginning of the Digital Age, the amount of
information produced in scientific research has increased geometrically each year. Consequently, new techniques have been developed to treat large datasets. However, data visualization has not experienced the same degree of development that mathematical tools have experienced, with  most visualizations still relying on carthesian representations and two dimensional graphs.
This approach might not be adequate anymore, especially when representing complex data with geographical components.
Considering that we are gradually shifting away from printed paper into screen-based interactions, the demand for tools that enable dynamic content visualization from anywhere continues to increase. Existing software for data visualization tipically lacks the customization options for geographic data representation and is not ready for the degree of interaction needed to display complex data.
On the other hand, current GIS software usually lacks the computational options to apply mathematical tools to large datasets to infer properties on a complex system, for example. Furthermore, GIS usually require a highly specialized training to operate and are not easy to approach for the data science community.
Usually, these software packages and online tools are not open source software and are limited by a commercial license.

In an attempt to make geospatial visualization accessible to an audience familiar with the computational tools used in the study of complex systems and large datasets, but with no prior knowledge of geomapping representations, I designed `OneWorld`. It uses the same input structure as many existing numerical [@numpy; @pandas] and graphical [@matplotlib; @seaborn] python packages in such a way that anyone that has used such computational tools in the past should have no difficulty in using `OneWorld`. This package includes highly customizable options to plot both static and interactive maps, presented in a simple and intuitive manner in order to make geospatial representation approachable to a broad audience.

# Overview

With a common input structure and operation, `OneWorld` can create both static figures for inclusion into documents and presentations, and interactive visualizations for embedding into web pages and online dashboards. To create static maps, `OneWorld` uses both `cartopy` [@cartopy] and `matplotlib` [@matplotlib] to create visualizations in the most common image formats. In order to produce interactive maps, `OneWorld` uses `jinja` [@jinja] to create an html document with embedded javascript which in turn uses `leaflet` [@leaflet] to create an interactive map. This whole process is transparent to the user, in such a way that the user needs only be concerned with writing the python script.

Like many other visualization packages in python, `OneWorld` uses an 'object-oriented' structure. The production of a map starts with the creation of a base map object which can be customized using different parameters. The user can then use the methods of this object to manipulate it and to add elements to the visualization (such as markers, lines, polygons, etc.). The visual characteristics of these elements, such as size and color, can be set to a common value for all elements or it can depend on the value of a variable.
When adding elements to a map, the user can pass an array-like variable containing the data for each element to the creator method, which will be used by the algorithm to set the size or color of each element. Furthermore, to maximize the flexibility on input variables, if the data is stored in a pandas DataFrame the names of its columns can be used as arguments in the creator methods as values for the coordinates, color and/or size of the elements.

The color argument accepts both numerical and categorical data, which is recognized and handled automatically by the algorithm, while the size argument requires numerical data. In both cases, a customizable legend is automatically added to the map. If numerical data is used to control the size of the elements added, `OneWorld` takes care of rescaling the values so that the elements displayed on the map have reasonable sizes, but the user is also given the option to use a custom size range. In the case of categorical data, the algorithm will adjust the number of colors in the color palette to match the number of categories in the variable.

The following code produces the static map shown in \autoref{fig:stat}.

```python
import pandas as pd
import oneworld as ow

mymap = ow.StaticMap(
        view=[-125, -66.5, 20, 50],
        central_longitude=-98.6,
        projection='AlbersEqualArea',
        inner_borders=['USA'])

df = pd.read_csv("example.csv")

mymap.add_markers(data=df,
                  latitude='Lat',
                  longitude='Long'
                  color='Expenditures',
                  palette='YlOrRd',
                  n_colors=6,
                  legend_show=False)
mymap.add_colorbar()

mymap.savemap("example.png")
```

![An example of a static map with markers. The coordinates of each marker correspond to the values in the 'Lat' and 'Long' columns of the 'df' pandas DataFrame. The color of each marker is determined by the values of the 'Expenditures' column  in the 'df' DataFrame. Instead of a legend, the map displays a colorbar. \label{fig:stat}](tut_markers4.png)


A special type of element that can be added to both static and interactive maps are geographic areas (administrative borders, population densities, etc.) in the form of a collection of polygons (or single polygon). For a static map, these polygons must be supplied in the form of a shapefile, while for an interactive map `OneWorld` requires a GeoJSON file. As with the other elements, the color of each area in the map can be set to a common value or to a variable (to produce a choropleth, for example).

When producing an interactive map, the elements added to the map can be created in a different layer (in a transparent overlay or in a base map), or added directly to the map object. If more than one layer is present in the map, a layer switch menu can be displayed. This gives control over what kind of data is displayed at a given time, as layers can be activated and deactivated in real time from the browser.
Mouse events can also be used to trigger changes in styles in the map or to display information on a panel, although currently only the 'mouseover' event is captured by the package. This feature can be used, for example, to change the color or the linewidth of an element in the map when the mouse hovers over it, or to display the value of a variable in an information panel.

As an example, the following script produces \autoref{fig:int}.

```python
import pandas as pd
import oneworld as ow

mymap = ow.WebMap(center = [39,-96.8], zoom = 4)

df = pd.read_csv("Regions.csv")

mymap.add_choropleth(data=df,
                     json_file='us_states.json', 
                     json_key='GEOID',
                     geoid='FIPS', 
                     color='Region',
                     style_mouseover = {'color': '#636363'},
                     info_mouseover = 'Name')

mymap.savemap("example.html")
```

![Example of an interactive choropleth. Polygons are read from the GeoJSON file. Each area is colored according to the value of the corresponding column 'Region' in the DataFrame 'df'. Note that the mouse pointer is positioned over Kansas in the map, which causes it to change its border color and to display its name as stored in the 'Name' column of 'df' in the information panel on the top right.\label{fig:int}](webmap_example.png)


# Availability

This package can be installed using `conda` through the `conda-forge` channel, which also installs its dependencies. It can also be installed through `pip`, provided that the requirements are satisfied. Alternatively, the source code can be donwloaded brom `Bitbucket` at https://bitbucket.org/taguilar/oneworld, and the documentation can be found at https://oneworld.readthedocs.io/.

# Acknowledgements

I would like to acknowledge Janire Pascual-Gonzalez for her invaluable contributions to the development of the software and the testing. I would also like to thank Thad Wasklewicz and Jay Golden for their support.

# References
